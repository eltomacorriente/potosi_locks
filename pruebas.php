<?php

include_once(__DIR__ . '/Command.php');
class Candado {
    public $imei = 0;
}
$input = "";
$candado  = new Candado();
main($input ,  $candado);

function main($input , $candado) {
    
    $command = new ReceivedCommand($input);
    echo "Tamaño: " . $command->length . "\n";
    echo "Protocolo: " . $command->protocol_information . "\n";
    echo "Data: " . json_encode($command->information_content) . "\n";
    // echo "serial Number: " . ($command->information_serial_number) . "\n";
    echo "CRC16: " . ($command->error_check) . "\n";
    //echo "is valid?: " . ($command->isCRCValid) . "\n";

    $output = "NON";

    switch ($command->protocol_number) {
        case "01":
            $output = loginCommand($command, $candado);
            break;

        case "23";
            $output = heartbeatCommand($command, $candado);
            break;

        case "32";
            $output = locationCommand($command, $candado);
            break;

        case "33";
            $output = alarmCommand($command, $candado);
            break;

        case "21";
            $output = terminalReponse($command, $candado);
            break;

        default :
            $output = "NON";
            break;
    }
    if ($output == "NON") {
        
    } else {
       // SocketServer::socket_write_smart($candado, $output);
    }
}

function pausa() {
    
}

function loginCommand(ReceivedCommand $command, &$candado) {
    echo "LOGIN \n ";
    $receivedLoginCommand = new ReceivedLoginCommand($command);


    echo "IMEI -> " . $receivedLoginCommand->imei . "\n";
    echo "MODEL -> " . $receivedLoginCommand->model . "\n";
    echo "TIME_ZONE -> " . $receivedLoginCommand->time_zone_language . "\n";

    $candado->singIn($receivedLoginCommand->model, $receivedLoginCommand->imei, 0);

    $reponseCommand = new ReponseLoginCommand();
    return $reponseCommand->getString2send();
}

function heartbeatCommand(ReceivedCommand $command, Candado &$candado) {
    echo "HEARTBEAT \n ";
    $receivedCommand = new ReceivedHearthbeatCommand($command);


    echo "terminal_information_content -> " . $receivedCommand->terminal_information_content . "\n";
    echo "Esta bloqueado? " . $receivedCommand->isLocked . "\n";
    echo "Esta localizado? " . (($receivedCommand->isGPSpositioning == 1 ) ? "si" : "no") . "\n";
    echo "Esta cargando? " . (($receivedCommand->isCharging == 1 ) ? "si" : "no") . "\n";
    echo "voltage_level -> " . $receivedCommand->voltage_level . "\n";
    echo "gsm_signal -> " . $receivedCommand->gsm_signal . "\n";
    echo "lenguage_port_status -> " . $receivedCommand->lenguage_port_status . "\n";

    $candado->Heartbeat($candado->deviceCode, $candado->IMEI, $receivedCommand->voltage_level, $receivedCommand->isLocked, $receivedCommand->gsm_signal);

    $reponseCommand = new BasicReponse($command->protocol_number);
    return $reponseCommand->getString2send();
}

function locationCommand(ReceivedCommand $command, Candado &$candado) {
    $receivedCommand = new ReceivedAlarmLocation($command);
    if ($receivedCommand->gps_location_length > 0) {
        echo "date_time -> " . $receivedCommand->date_time . "\n";
        echo "gps_location_length -> " . $receivedCommand->gps_location_length . "\n";
        echo "satelites_number -> " . $receivedCommand->satelites_number . "\n";
        echo " latitude -> " . $receivedCommand->latitude . "\n";
        echo " longitude -> " . $receivedCommand->longitude . "\n";
        $candado->setLatLng($receivedCommand->latitude, 'N', $receivedCommand->longitude, 'W', $candado->IMEI);
    }

    $reponseCommand = new BasicReponse($command->protocol_number);
    return $reponseCommand->getString2send();
}

function alarmCommand(ReceivedCommand $command, Candado &$candado) {

    $receivedCommand = new ReceivedAlarmLocation($command);

    if ($receivedCommand->gps_location_length > 0) {
        echo "date_time -> " . $receivedCommand->date_time . "\n";
        echo "gps_location_length -> " . $receivedCommand->gps_location_length . "\n";
        echo "satelites_number -> " . $receivedCommand->satelites_number . "\n";
        echo " latitude -> " . $receivedCommand->latitude . "\n";
        echo " longitude -> " . $receivedCommand->longitude . "\n";
        $candado->setLatLng($receivedCommand->latitude, 'N', $receivedCommand->longitude, 'W', $candado->IMEI);
    }
    echo "status -> " . $receivedCommand->status . "\n";
    echo "status name -> " . $receivedCommand->status_name . "\n";
    $reponseCommand = new BasicReponse($command->protocol_number);
    return $reponseCommand->getString2send();
}

function terminalReponse(ReceivedCommand $command, Candado &$candado) {

    $receivedCommand = new TerminalReponse($command);
    echo " RECIBIDO -> " . $receivedCommand->command;

    if (strpos($receivedCommand->command, "Current position") !== false) {

        $command = trim(str_replace("Current position! ", "", $receivedCommand->command));
        $command_array = explode(",", $command);


        $lat_dec = trim(str_replace("Lat:N", "", $command_array[0]));
        $lat_deg = floor($lat_dec);
        $lat_min = (($lat_dec - $lat_deg ) * 60);
        if ($lat_min >= 10) {
            $latitude = $lat_deg . "" . $lat_min;
        } else {
            $latitude = $lat_deg . "0" . $lat_min;
        }

        $lng_dec = trim(str_replace("Lon:W", "", $command_array[1]));
        $lng_deg = floor($lng_dec);   // 4
        $lng_min = (($lng_dec - $lng_deg ) * 60);  // 4.07 - 4   * 60 
        if ($lng_min >= 10) {
            $longitude = $lng_deg . "" . $lng_min;
        } else {
            $longitude = $lng_deg . "0" . $lng_min;
        }
        echo "\n";
        echo " latitude -> " . $latitude . "\n";
        echo " longitude -> " . $longitude . "\n";

       // $candado->setLatLng($latitude, 'N', $longitude, 'W', $candado->IMEI);
    }

    return "NON";
}
