<?php
date_default_timezone_set('America/Bogota');
require_once("SocketServer.class.php");

class Candado extends SocketServerClient {

    public $deviceCode = '';
    public $IMEI = '';
    public $lockStatus = '';
    public $batteryStatus = 0;
    public $gpsSignal = 0;
    public $version = 0;
    public $ICCID = 0;
    public $firmware = 0;
    public $compileTime = 0;
    public $position = 0;
    public $reserveDataInStatus1 = 0;
    public $reserveDataInStatus2 = 0;
    public $userID = 0;
    public $unlockTime = 0;
    public $ridingTime = 0;
    public $alarmStatus = 0;
    public $ringSeconds = 0;
    public $reserveParameterRing = 0;
    public $date_pos;

    public function __construct(&$socket, $i) {
        parent::__construct($socket, $i);
        $this->date_pos = time();
    }

    public function __construct1($dc, $imei, $btery) {

        $this->deviceCode = $dc;
        $this->IMEI = $imei;
        $this->batteryStatus = $btery;
    }

    public function aMemberFunc() {
        print 'Inside `aMemberFunc()`';
    }

    function __toString() {
        //return "Socket: " . $this->socket . " IP: " . $this->ip . " HostName: " . $this->hostname . " ServerClientIndex: " . $this->server_clients_index . "\n \n";
        $timeElepsed = floor((time() - $this->timeConection ) / 60);
        return $this->server_clients_index . "\t" . $this->ip . "\t" . $this->IMEI . "\t" . $timeElepsed;
    }

    public function singIn($dc, $imei, $btery) {
        $this->deviceCode = $dc;
        $this->IMEI = $imei;
        $this->batteryStatus = $btery;
    }

    public function Heartbeat($dc, $imei, $btery, $statusLock, $gsm) {
        $this->deviceCode = $dc;
        $this->IMEI = $imei;
        $this->batteryStatus = $btery;

        try {
            $usuario = "user_bicico";
            $contrasena = "firewallTest123";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
            $servidor = "rodo.bike";
            $basededatos = "pruebabicico";
            $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
            date_default_timezone_set('America/Bogota');
            $fecha = date("Y-m-d H:i:s");
            $consulta = "UPDATE locks SET battery_status =$btery, gps_signal ='$gsm', status ='$statusLock', date_status='$fecha' WHERE IMEI = '$imei'";
            $resultado = mysqli_query($conexion, $consulta) ;
            mysqli_close($conexion);
        } catch (Exception $e) {
            echo "Error al actualizar heartbeat";
        }
    }

    public function setFirmware($fm, $comtime) {
        $this->firmware = $fm;
        $this->compileTime = $comtime;
    }

    public function setICCID($icSim) {
        $this->ICCID = $icSim;
    }

    public function setPOS($pos) {
        $this->position = $pos;
    }

    public function setStatus($imei, $bt, $gsm, $resrstat1, $lockStat, $resrstat2 = 0) {
        $this->IMEI = $imei;
        $this->batteryStatus = $bt;
        $this->gpsSignal = $gsm;
        $this->reserveDataInStatus = $resrstat1;
        $this->lockStatus = $lockStat;
        $this->reserveDataInStatus2 = $resrstat2;

        "\n reserveDataInStatus: " . $resrstat1 . "\n   lockStatus: " . $lockStat .
                "\n reserveDataInStatus2: " . $resrstat2 . "\n" .
                "\n";

        try {
            $usuario = "user_bicico";
            $contrasena = "firewallTest123";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
            $servidor = "rodo.bike";
            $basededatos = "pruebabicico";
            $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
            date_default_timezone_set('America/Bogota');
            $fecha = date("Y-m-d H:i:s");
            $consulta = "UPDATE locks SET battery_status = '$bt', gps_signal = '$gsm', status = '$lockStat', date_status='$fecha' WHERE IMEI = '$imei'";
            $resultado = mysqli_query($conexion, $consulta) ;
            mysqli_close($conexion);
        } catch (Exception $e) {
            echo "Error al actualizar Status";
        }
    }

    public function unlock($imei, $ls, $UI, $unlTim) {// 0 abrir
        $this->lockStatus = $ls;
        $this->userID = $UI;
        $this->unlockTime = $unlTim;
        if ($ls == '0') {
            try {
                $usuario = "user_bicico";
                $contrasena = "firewallTest123";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
                $servidor = "rodo.bike";
                $basededatos = "pruebabicico";
                $consulta = "UPDATE locks SET status = '$ls' WHERE IMEI = '$imei'";
                //echo $consulta;
                $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
                $resultado = mysqli_query($conexion, $consulta);
                mysqli_close($conexion);
            } catch (Exception $e) {
                echo "Error al actualizar unlock";
            }
        } else {
            
        }
    }

    public function lock($imei, $UI, $unlTim, $rt) {
        $ls = 1;
        $this->lockStatus = $ls;
        $this->userID = $UI;
        $this->unlockTime = $unlTim;
        $this->ridingTime = $rt;
        if ($ls == '1') {
            try {
                $usuario = "user_bicico";
                $contrasena = "firewallTest123";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
                $servidor = "rodo.bike";
                $basededatos = "pruebabicico";
                $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
                $consulta = "UPDATE locks SET status = '$ls' WHERE IMEI = '$imei'";
                $resultado = mysqli_query($conexion, $consulta);
                mysqli_close($conexion);
            } catch (Exception $e) {
                echo "Error al actualizar lock";
            }
            //$imei = 866855039153473;
            //$URL = "http://localhost:8000/api/endFromLock";
            // abrimos la sesión cURL
            $ch = curl_init();

            // definimos la URL a la que hacemos la petición
            curl_setopt($ch, CURLOPT_URL, "http://rodo.bike/api/endFromLock");
            // indicamos el tipo de petición: POST
            curl_setopt($ch, CURLOPT_POST, TRUE);

            //echo $imei;
            // definimos cada uno de los parámetros
            curl_setopt($ch, CURLOPT_POSTFIELDS, "imei=$imei");
            // recibimos la respuesta y la guardamos en una variable
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $remote_server_output = curl_exec($ch);



            // hacemos lo que queramos con los datos recibidos
            // por ejemplo, los mostramos
            // print_r($remote_server_output);
            // cerramos la sesión cURL
            curl_close($ch);
        } else {
            
        }
    }

    public function ringing($secring, $rp) {
        $this->ringSeconds = $secring;
        $this->reserveParameterRing = $rp;
        //  echo "ring!\n   ringSeconds:" . $secring . "\n   reserveParameterRing: " . $rp . "\n";
    }

    public function alarm($AS) {
        $this->alarmStatus = $AS;
        //   echo "Alarm!\n   alarmStatus:" . $AS . "\n";
    }

    public function getStatus() {
        //funcion no usada
    }

    public function setLatLng($lat, $latNS, $lng, $lngWE, $imei) {
        $this->IMEI = $imei;
        $latitude = floatval($lat) / 100;
        $longitude = floatval($lng) / 100;
        if ($latNS == 'S') {
            $latitude = ($latitude) * -1;
        }
        if ($lngWE == 'W') {
            $longitude = ($longitude) * -1;
        }
        if ($latitude == 0) {
            
        } else {
            try {
                $usuario = "user_bicico";
                $contrasena = "firewallTest123";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
                $servidor = "rodo.bike";
                $basededatos = "pruebabicico";
                $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
                date_default_timezone_set('America/Bogota');
                $fecha = date("Y-m-d H:i:s");
                try {
                    add2Trajectories($imei, $latitude, $longitude, $fecha);
                } catch (Exception $e) {
                    echo "Error al llamar el metodo para actualizar tracyectorias";
                }
                $this->date_pos = time();

                $consulta = "UPDATE locks SET lat =$latitude, lng =$longitude, date_pos='$fecha' WHERE IMEI = '$imei'";
                $resultado = mysqli_query($conexion, $consulta) ;
                mysqli_close($conexion);
            } catch (Exception $e) {
                echo "Error al actualizar Posicion";
            }
        }
    }

}

function add2Trajectories($imei, $lat, $lng, $date) {
    try {
        $usuario = "user_bicico";
        $contrasena = "firewallTest123";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
        $servidor = "rodo.bike";
        $basededatos = "pruebabicico";
        $conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
        $sql = " 
			SELECT  rides.id as rides_id                        
			FROM rides			
			left join locks on rides.lock_id = locks.id 			
			WHERE ( rides.status = 'in progress' AND locks.IMEI = $imei ) 
			";
        $resultado = mysqli_query($conexion, $sql) ;
        while ($fila = mysqli_fetch_array($resultado)) {
            $ride_id = $fila["rides_id"];
            $latDeg = (int) $lat;
            $latMin = abs(($latDeg - $lat) * 100);
            $lngDeg = (int) $lng;
            $lngMin = abs(($lng - $lngDeg) * 100);
            $latLock = $latDeg + ($latMin / 60);
            $lngLock = $lngDeg - ($lngMin / 60);
            try {
                $conexion2 = mysqli_connect($servidor, $usuario, $contrasena, $basededatos);
                $sql2 = " 
			INSERT INTO trajectories (lat, lng, date, ride_id)
			VALUES ( '$latLock' , '$lngLock' , '$date' , '$ride_id' )
			";
                $resultado2 = mysqli_query($conexion2, $sql2);
                mysqli_close($conexion2);
            } catch (Exception $e) {
                 echo "Error al actualizar la tracyectorie";
            }
        }
        mysqli_close($conexion);
    } catch (Exception $e) {
        echo "Error al buscar rides activos";
    }
}
