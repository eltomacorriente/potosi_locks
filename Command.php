<?php

/*
 * 7878  -> INICIO
 * 11 -> Tamaño en hexa es 17
 * 01 -> comando login
 * 0355951092138416 -> imei  de tamaño 8
 * 3608 -> Modelo creo que este corresponde a BL10 
 * 1F4A -> time zone lenguage  buuuufffffff
 * 0009 -> Information Serial Number 
 * 755D -> Error Check
 * 0D0A -> FIN
 * 
 */

abstract class Command {

    const init1Byte = "7878";
    const init2Byte = "7979";
    const end = "0D0A";

    public $length;  // 1 a 2 bytes
    public $protocol_number; // 1 byte
    public $information_content; // $information_content_lenght bytes
    public $information_serial_number;
    public $error_check; // 2 bytes
    public $isCRCValid;
    public $information_content_lenght; // N 
    public $protocol_information; // N 
    public static $protocoles = [
        "00" => "Indefinido",
        "01" => "01 - Login information",
        "23" => "23 - Heartbeat packet",
        "21" => "21 - Online command response by terminal",
        "2C" => "2C - Wifi Packet",
        "32" => "32 - GPS location information",
        "33" => "33 - Location information (alarm)",
        "80" => "80 - Online command",
        "98" => "98 - Information Transmission Packet",
    ];

    public function __construct() {
        $this->length = 5;  // 1 a 2 bytes
        $this->protocol_number = "00"; // 1 byte
        $this->protocol_information = self::$protocoles[$this->protocol_number]; // N 
        $this->information_content = array(); // $information_content_lenght bytes
        $this->error_check = "0000"; // 2 bytes         
        $this->information_content_lenght = 0; // N 
        $this->information_serial_number = 1;
    }

}

class ReceivedCommand extends Command {

    protected $offset;
    protected $lengthSize;
    protected $input;

    public function __construct($input) {
        parent::__construct();
        $this->input = $input;
        $commandFromLock = strToHex($input); // Example  78781101035595109213841636081F4A0009755D0D0A
        ////$commandFromLock = "7979001101035595109213841636081F4A0009755D0D0A"; // login
        //$commandFromLock = "78780B23C00122040001000818720D0A"; // heartbeat
        //$commandFromLock = str_replace(' ', '', "79 79 00 6F 33 11 03 14 09 06 08 00 09 01 CC 00 28 7D 00 1F 40 0E 24 28 7D 00 1F 71 07 28 7D 00 1E 3F 06 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 31 00 36 76 05 BB 5D 46 00 87 36 31 87 5B 48 CC 7B 35 36 61 A6 4C 00 E0 4B 8C BF 58 4F 78 A1 06 54 15 DE 4F 00 87 46 1B 9D 84 51 26 52 F3 AD B1 94 55 A1 00 00 08 38 B2 0D 0A"); // location alarm 
      //  $commandFromLock = "7979007221000000000143757272656E7420706F736974696F6E21204C61743A4E342E3539333632372C4C6F6E3A5737342E3131353235302C436F757273653A3235322E31352C53706565643A312E39384B6D2F682C4461746554696D653A323031392D30382D30362031333A33333A3031002CC4420D0A";
        echo "En hex: " . $commandFromLock . "\n";
        $dividedCommandHEX = divideInPairs($commandFromLock);
        $this->setLengthSize($dividedCommandHEX[0] . $dividedCommandHEX[1]);
        $this->setLength($dividedCommandHEX[2], $dividedCommandHEX[3]);
        $this->setProtocol($dividedCommandHEX[3 + $this->offset]);
        $this->setContent($dividedCommandHEX);
        $this->setSerialNumber($dividedCommandHEX[$this->information_content_lenght + 4 + $this->offset] . $dividedCommandHEX[$this->information_content_lenght + 5 + $this->offset]);
        $this->setCRC($dividedCommandHEX[$this->information_content_lenght + 6 + $this->offset] . $dividedCommandHEX[$this->information_content_lenght + 7 + $this->offset]);
    }

    private function setLengthSize($init) {

        if ($init == self::init1Byte) {
            $this->lengthSize = 1;
            $this->offset = 0;
        } else if ($init == self::init2Byte) {
            $this->lengthSize = 2;
            $this->offset = 1;
        }
    }

    private function setLength($l1, $l2) {
        if ($this->lengthSize == 1) {
            $this->length = hexdec($l1);
        } else {
            $this->length = hexdec($l1 . $l2);
        }
    }

    private function setProtocol($p) {
        foreach (Command::$protocoles as $protocol_number => $protocol) {
            if ($p == $protocol_number) {
                $this->protocol_information = $protocol;
                $this->protocol_number = $protocol_number;
            }
        }
    }

    private function setContent($data) {
        $this->information_content_lenght = $this->length - 5;
        $this->information_content = array_slice($data, 4 + $this->offset, $this->information_content_lenght);
    }

    private function setSerialNumber($sn) {
        $this->information_serial_number = hexdec($sn);
    }

    private function setCRC($crc) {
        $this->error_check = $crc;
        $this->isCRCValid = $this->isCRCValid($crc);
    }

    private function isCRCValid($crc2check) {
        $generatedCRC = CRC::getCRC("1101035595109213841636081F4A0009");
        return ($generatedCRC == $crc2check);
    }

    protected function extractValue($init, $size) {
        $retournable = "";
        $iteration = $init;
        while ($iteration < ( $size + $init)) {
            $retournable .= $this->information_content[$iteration++];
        }

        return $retournable;
    }

}

abstract class SendingCommand extends Command {

    public static $information_serial_number_sending = 1;  // 2 bytes

    public abstract function setInit();

    public abstract function setInformationContent();

    public abstract function setCommand();

    private function calculateSize($info_content) {
        $init = $this->init;
        $length = strtoupper(dechex(((strlen($info_content) / 2) + 5)));

        if (strlen($length) == 1 || strlen($length) == 3) {
            $length = "0" . $length;
        } else {
            $length = $length;
        }

        if ($init == self::init1Byte && strlen($length) == 2) {
            return $length;
        } else if ($init == self::init2Byte && strlen($length) == 2) {
            return "00" . $length;
        } else if ($init == self::init1Byte && strlen($length) == 4) {
            $this->init = self::init2Byte;
            return $length;
        } else {
            return $length;
        }
    }

    public function getCommandID() {
        $sn = dechex(self::$information_serial_number_sending++);
        echo "HHEEYY  " . $sn . "\n";
        if (strlen($sn) == 1) {
            $sn = "000" . $sn;
        } else if (strlen($sn) == 2) {
            $sn = "00" . $sn;
        } else if (strlen($sn) == 3) {
            $sn = "0" . $sn;
        } else if (strlen($sn) > 4) {
            self::$information_serial_number_sending = 0;
            $sn = self::$information_serial_number_sending;
        }
        return $sn;
    }

    public function getString2send() {
        $this->init = $this->setInit();
        $this->information_content = $this->setInformationContent();
        $this->command = $this->setCommand();

        $this->length = $this->calculateSize($this->information_content);

        $commandID = $this->getCommandID();
        $this->crc = CRC::getCRC($this->length . $this->command . $this->information_content . $commandID);

        $string = $this->init . $this->length . $this->command . $this->information_content . $commandID . $this->crc . self::end;

        echo "ESTOY ENVIANDO : " . $string . "\n";

        return hexToStr($string);
    }

}

// GENERAL REPONSES

class BasicReponse extends SendingCommand {

    private $commandReponse;

    public function __construct($cr) {
        $this->commandReponse = $cr;
        parent::__construct();
    }

    public function setInit() {
        return self::init1Byte;
    }

    public function setInformationContent() {
        return "";
    }

    public function setCommand() {
        return $this->commandReponse;
    }

}

// LOGIN

class ReceivedLoginCommand extends ReceivedCommand {

    public $imei;
    public $model;
    public $time_zone_language;
    protected $imei_size = 8;
    protected $model_size = 2;
    protected $time_zone_language_size = 2;

    public function __construct(ReceivedCommand $command) {
        $objValues = get_object_vars($command); // return array of object values
        foreach ($objValues AS $key => $value) {
            $this->$key = $value;
        }

        $this->extractData();
    }

    public function extractData() {
        $this->imei = "";
        $this->model = "";
        $this->time_zone_language = "";
        $this->imei = (int) $this->extractValue(0, $this->imei_size);
        $this->model = $this->extractValue($this->imei_size, $this->model_size);
        $this->time_zone_language = $this->extractValue($this->model_size + $this->imei_size, $this->time_zone_language_size);
    }

}

class ReponseLoginCommand extends SendingCommand {

    public function setInit() {
        return self::init1Byte;
    }

    public function setInformationContent() {
        date_default_timezone_set('UTC');
        $formatedDate = date("ymdHis");
        $reservedValueSize = "00";
        $reservedValue = "";
        return $formatedDate . $reservedValueSize . $reservedValue;
    }

    public function setCommand() {
        return "01";
    }

}

// HEARTBEAT

class ReceivedHearthbeatCommand extends ReceivedCommand {

    public $isLocked;
    public $isCharging;
    public $isGPSpositioning;
    public $terminal_information_content;
    protected $terminal_information_content_length = 1;
    public $voltage_level;
    protected $voltage_level_length = 2;
    public $gsm_signal;
    protected $gsm_signal_length = 1;
    public $lenguage_port_status;
    protected $lenguage_port_status_length = 2;

    public function __construct(ReceivedCommand $command) {
        $objValues = get_object_vars($command); // return array of object values
        foreach ($objValues AS $key => $value) {
            $this->$key = $value;
        }

        $this->extractData();
    }

    public function extractData() {

        // Getting values from information content
        $terminal_information_content = $this->extractValue(0, $this->terminal_information_content_length);
        $this->terminal_information_content = hextobin($terminal_information_content);
        echo "\n BYTE INFO" . $this->terminal_information_content . "\n";

        $this->isLocked = substr($this->terminal_information_content, 7, 1);
        $this->isCharging = substr($this->terminal_information_content, 5, 1);
        $this->isGPSpositioning = substr($this->terminal_information_content, 2, 1);


        // Getting value of battery level
        $voltage_level_hex = $this->extractValue($this->terminal_information_content_length, $this->voltage_level_length);
        $this->voltage_level = hexdec($voltage_level_hex);

        // Getting value of gsm signal
        $this->gsm_signal = $this->extractValue(
                $this->terminal_information_content_length + $this->voltage_level_length, $this->gsm_signal_length);


        // Getting value of gsm signal
        $this->lenguage_port_status = $this->extractValue(
                $this->terminal_information_content_length + $this->voltage_level_length + $this->gsm_signal_length, $this->lenguage_port_status_length);
    }

}

// ALARM LOCATION

class ReceivedAlarmLocation extends ReceivedCommand {

    public function __construct(ReceivedCommand $command) {
        $objValues = get_object_vars($command); // return array of object values
        foreach ($objValues AS $key => $value) {
            $this->$key = $value;
        }

        $this->extractData();
    }

    public function extractData() {

        // Getting values from information content
        $offset = 0;
        $this->date_time = $this->extractValue($offset, 6);
        $this->gps_location_length = hexdec($this->extractValue($offset += 6, 1));

        if ($this->gps_location_length > 0) {
            $this->extractGPSdata($offset);
        }
        $this->main_base_station_length = hexdec($this->extractValue($offset += (1 + $this->gps_location_length), 1));
        if ($this->main_base_station_length > 0) {
            /*
             * MCC 2
             * MNC 1
             * LAC 2
             * CI 3
             * RSSI 1
             */
        }
        $this->sub_base_station_length = hexdec($this->extractValue($offset += (1 + $this->main_base_station_length), 1));
        if ($this->sub_base_station_length > 0) {
            /*
              NLAC1 2 Same as LAC
              NCI1 3 Same as CI
              NRSSI1 1 Same as RSSI
              NLAC2 2 Same as LAC
              NCI2 3 Same as CI
              NRSSI2 1 Same as RSSI
             */
        }
        $this->wifi_length = hexdec($this->extractValue($offset += (1 + $this->sub_base_station_length), 1));
        if ($this->wifi_length > 0) {
            /*
              WIFI Message
              Length
              1
              The WIFI information length should be 7 or the
              multiple of 7. Eg: 7 means sending one WIFI message
              while 14 means sending 2 WIFI messages. No
              transmission if it is 0. WIFI MAC1 6
              MAC of received signal 1’s WIFI（transmit by
              searched WIFI quantity. For example, transmit one if
              one WIFI searched; transmit several WIFI if several
              WIFI searched. No transmission if no WIFI)
              WIFI Strength 1 1 WIFI signal strength of 1
              WIFI MAC2 6 Same as above
              WIFI Strength 2 1 Same as above
             */
        }
        $this->status = $this->extractValue($offset += (1 + $this->wifi_length), 1);
        $this->status_name = $this->getStatusName($this->status);

        $this->reserved_length = hexdec($this->extractValue($offset += 1, 1));
        if ($this->reserved_length > 0) {
            /*
              NLAC1 2 Same as LAC
              NCI1 3 Same as CI
              NRSSI1 1 Same as RSSI
              NLAC2 2 Same as LAC
              NCI2 3 Same as CI
              NRSSI2 1 Same as RSSI
             */
        }
    }

    public function extractGPSdata($offset) {
        $this->satelites_number = $this->extractValue($offset += 1, 1);

        $lat_hex = $this->extractValue($offset += 1, 4);
        $lat_dec = (hexdec($lat_hex)) / 1800000;
        $lat_deg = floor($lat_dec);
        $lat_min = (($lat_dec - $lat_deg ) * 60);
        if ($lat_min >= 10) {
            $this->latitude = $lat_deg . "" . $lat_min;
        } else {
            $this->latitude = $lat_deg . "0" . $lat_min;
        }


        $lng_hex = $this->extractValue($offset += 4, 4);
        $lng_dec = (hexdec($lng_hex)) / 1800000;  // 4.07
        $lng_deg = floor($lng_dec);   // 4
        $lng_min = (($lng_dec - $lng_deg ) * 60);  // 4.07 - 4   * 60 
        if ($lng_min >= 10) {
            $this->longitude = $lng_deg . "" . $lng_min;
        } else {
            $this->longitude = $lng_deg . "0" . $lng_min;
        }




        $speed = $this->extractValue($offset += 4, 1);
        $this->speed = hexdec($speed);
        $this->course = $this->extractValue($offset += 1, 2);
        $offset++;  // -> para que esten a la mismo nivel.
    }

    private function getStatusName($status) {
        switch ($status) {
            case "00":
                return "timing report";
            case "01":
                return "report in fixed distance";
            case "02":
                return "re-upload GPS data";
            case "0B":
                return "LJDW report";
            case "A0":
                return "Lock report";
            case "A1":
                return "Unlock report";
            case "A2":
                return "Low internal battery alarm";
            case "A3":
                return "Low battery and shutdown alarm";
            case "A4":
                return "Abnormal alarm";
            case "A5":
                return "Abnormal unlocking alarm";
        }

    }

}

// OPEN LOCK


class OpenLockCommand extends SendingCommand {

    public function setInit() {
        return self::init1Byte;
    }

    public function setInformationContent() {
        $comandLength = "0B";
        $serverIdentificacion = "00000000";
        $unlockCommnadASCII = "554E4C4F434B23";  //55 4E 4C 4F 43 4B 23   UNLOCK#        
        return $comandLength . $serverIdentificacion . $unlockCommnadASCII;
    }

    public function setCommand() {
        return "80";
    }

}

// GET LOCATION

class LocationCommand extends SendingCommand {

    public function setInit() {
        return self::init1Byte;
    }

    public function setInformationContent() {
        $comandLength = "0A";
        $serverIdentificacion = "00000000";
        $unlockCommnadASCII = "574845524523";  //57 48 45 52 45 23   WHERE#        
        return $comandLength . $serverIdentificacion . $unlockCommnadASCII;
    }

    public function setCommand() {
        return "80";
    }

}

// TERMINAL REPONSE

class TerminalReponse extends ReceivedCommand {

    public $command;

    public function __construct(ReceivedCommand $command) {
        $objValues = get_object_vars($command); // return array of object values
        foreach ($objValues AS $key => $value) {
            $this->$key = $value;
        }

        $this->extractData();
    }

    public function extractData() {
        $serverIdentification = $this->extractValue(0, 4);
        $serflag = $this->extractValue(4, 1);
        $this->command = hexToStr($this->extractValue(5, $this->information_content_lenght - 5));

       
    }

}

// AUXILIAR CLASSES AND FUNCTIONS
class CRC {

    private static $CRC16_Table = [0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF, 0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7, 0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E, 0x9CC9, 0x8D40,
        0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876, 0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD, 0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5, 0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E,
        0x54B5, 0x453C, 0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974, 0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB, 0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3, 0x5285, 0x430C,
        0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A, 0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72, 0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9, 0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3,
        0x8A78, 0x9BF1, 0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738, 0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70, 0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7, 0x0840, 0x19C9,
        0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF, 0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036, 0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E, 0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7,
        0xC03C, 0xD1B5, 0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD, 0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134, 0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C, 0xC60C, 0xD785,
        0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3, 0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB, 0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232, 0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68,
        0x3FF3, 0x2E7A, 0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1, 0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9, 0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330, 0x7BC7, 0x6A4E,
        0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78];

    public static function getCRC($pData) {
        //0$pData = str_replace(' ', '', " 0B 23 C0 01 22 04 00 01 00 08 ");
        $hexdata = pack('H*', $pData);
        $nLength = strlen($hexdata);
        $fcs = 0xFFFF;
        $pos = 0;
        while ($nLength > 0) {
            $fcs = ($fcs >> 8) ^ self::$CRC16_Table[($fcs ^ ord($hexdata[$pos])) & 0xFF];
            $nLength--;
            $pos++;
        }
        return strtoupper(substr(dechex(~$fcs), -4, 4));
    }

}

function myHexToStr($hex) {
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= (hexdec($hex[$i] . $hex[$i + 1])) . " ";
    }
    return $string;
}

function divideInPairs($hex) {
    $output = array();
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        array_push($output, $hex[$i] . $hex[$i + 1]);
    }
    return $output;
}

function strToHex($string) {
    $hex = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0' . $hexCode, -2);
    }
    return strToUpper($hex);
}

function hexToStr($hex) {
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
    }
    return $string;
}

function shexdec($h) {
    return ($h[0] === '-') ? -('0x' . substr($h, 1) + 0) : ('0x' . $h + 0);
}

function hextobin($data) {

    $out = base_convert($data, 16, 2);

    for ($i = strlen($out); $i <= 7; $i++) {
        $out = "0" . $out;
    }

    return $out;
}
