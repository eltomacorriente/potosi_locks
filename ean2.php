<?php

if (isset($_GET["email"])) {
    $tocken = getTocken();
    echo sendEmail($_GET["email"], $tocken);
} else {
    
}

function prepareHeaders($headers) {
    $flattened = array();

    foreach ($headers as $key => $header) {
        if (is_int($key)) {
            $flattened[] = $header;
        } else {
            $flattened[] = $key . ': ' . $header;
        }
    }

    return implode("\r\n", $flattened);
}

function getTocken() {
    $url = 'http://172.16.0.32:9009/api/AccesService';
    $data = array('param1' => 'bicycleean' , 'param1' => 'eanbicycle2019');

    $headers = array(
        'Content-Type' => 'application/json',
        'Authorization' => $tocken,
    );

    $options = array(
        'http' => array(
            'header' => prepareHeaders($headers),
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;
}

function sendEmail($email, $tocken) {
    $url = 'http://172.16.0.32:9009/api/AccesLDAP';
    $data = array('param1' => $email);

    $headers = array(
        'Content-Type' => 'application/json',
        'Authorization' => $tocken,
    );

    $options = array(
        'http' => array(
            'header' => prepareHeaders($headers),
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    return $result;
}
